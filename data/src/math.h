#pragma once

class Point2D {
 public:
  Point2D(float, float);
  ~Point2D();
  void setPosition(float);
  void setPosition(float, float);
  void setPosition(Point2D);
  float getX();
  float getY();

 private:
  float position_x;
  float position_y;
};
