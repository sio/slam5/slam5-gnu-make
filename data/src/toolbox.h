#pragma once
#include <iostream>

class Tool_One {
 public:
  Tool_One(std::size_t);
  ~Tool_One();
  void SuperFunction();
  
 private:
  std::size_t max_iteration;
};
